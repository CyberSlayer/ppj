#include <iostream>
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
using namespace std;


int main()
{
 char blue[] = { 0x1b, '[', '1', ';', '3', '4', 'm', 0 };
 char normal[] = { 0x1b, '[', '0', ';', '3', '9', 'm', 0 };
 struct enemy
 {
   int health;
   int damage; 
   int xp;
 };
 struct character
 {
   int health;
   int damage;
   int xp;
   int level;
 };
 character character;
 character.health = 100;
 character.damage = 10;
 character.xp = 0;
 character.level = 1;
 enemy Skeleton;
 Skeleton.health = 10;
 Skeleton.damage = 2;
 Skeleton.xp = 10;
 
  cout << "welcome to my game!\n";
  char username[25];
  cout << "please tell me your name: ";
  cin >> username;
  cout << "so your name is " << username << "? ";
  string a;
  cin >> a;
  if(a == "yes")
  {
      cout << "that's a great name!\n";
      sleep(2);
  }
  else if (a == "no")
  {
      cout << "so what's your name again? ";
      char username2[25];
      cin >> username2;
      cout << "so it's " << username2 << "...";
      sleep(2);
      cout << "great! Let's start our adventure!\n";
  }
  cout << "let's not waste any more time!\n";
  sleep(1);
  cout << "you step out of your door and see a skeleton,you firmly hold on your sword's grip and go for him\n";
  sleep(1);
  cout << "you quietly approach him to take him down from behind\n";
  while(character.health > 0 && Skeleton.health > 0)
  {
   cout << "you have " << character.health << " HP\n";
   sleep(2);
   cout << "the skeleton has " << Skeleton.health << " HP\n";
   cout << "you decide to attack the Skeleton\n";
   sleep(2);
   Skeleton.health = Skeleton.health - character.damage; 
  } 
  if(character.health <= 0)
  {
    cout << "you died\n";
    return 0;
  }
  if(Skeleton.health <= 0)
  {
    cout << "you won!! and gained " << Skeleton.xp << " XP!\n";
    character.xp = Skeleton.xp + character.xp;
  } 
  sleep(2);
  cout << "you continue your journey and stumble upon a tavern\n";
  sleep(2);
  cout << "you decide to rest there for a while...\n";
  sleep(2);
  cout << "menu\n";
  cout << "Choose option: \n";
  cout << "|1|- buy potions\n";
  cout << "|2|- rest\n";
  cout << "|3|- settings\n";
  cout << "|4|- quit\n";
  string i;
  cin >> i;
}
